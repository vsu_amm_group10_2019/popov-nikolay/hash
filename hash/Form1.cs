﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.Json;

namespace hash
{
    public partial class Form1 : Form
    {
        const int size = 10;
        HashTable table = new HashTable(size);
        private string FileName { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        public void TableMake()
        {
            List<Tabel> tabels = table.GetAll();
            dataGridView1.Rows.Clear();
            dataGridView1.RowCount = tabels.Count;
            for (int i = 0; i < tabels.Count; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = tabels[i].tabNumber;
                dataGridView1.Rows[i].Cells[1].Value = tabels[i].FIO;
                dataGridView1.Rows[i].Cells[2].Value = tabels[i].salary;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Json File|*.json";
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                SaveToFile(saveFileDialog1.FileName);
            }
        }
        private void SaveToFile(string fileName)
        {
            List<Tabel> tabels = table.GetAll();
            File.WriteAllText(fileName, JsonSerializer.Serialize<List<Tabel>>(tabels));
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile(FileName);
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Json File|*.json";
            if (openFileDialog1.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                FileName = openFileDialog1.FileName;
                List<Tabel> tabels = JsonSerializer.Deserialize<List<Tabel>>(File.ReadAllText(FileName));
                table.Clear();
                foreach (Tabel t in tabels)
                {
                    table.Add(t);
                }
                TableMake();
            }
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileName = "";
            table.Clear();
            TableMake();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TabelForm tabelForm = new TabelForm();
            tabelForm.ShowDialog();
            if (tabelForm.isRightClosed)
            {
                if (table.Search(tabelForm.t.tabNumber) != null)
                {
                    MessageBox.Show("Табельный номер уже существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (tabelForm.t.tabNumber == "" || tabelForm.t.FIO == "")
                    {
                        MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        table.Add(tabelForm.t);
                        TableMake();
                    }
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AskForm askForm = new AskForm();
            askForm.ShowDialog();
            if (askForm.num != "")
            {
                Tabel t = table.Search(askForm.num);
                if (t == null)
                {
                    MessageBox.Show("Табельный номер не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    table.Delete(t);
                    TableMake();
                }
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AskForm askForm = new AskForm();
            askForm.ShowDialog();
            if (askForm.num != "")
            {
                Tabel t = table.Search(askForm.num);
                if (t == null)
                {
                    MessageBox.Show("Табельный номер не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"ФИО: {t.FIO}\nЗарплата: {t.salary}", "Табель", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("4. Задан набор записей: табельный номер, ФИО, зарплата.\nПо табельному номеру вывести остальные сведения.\nМетод разрешения коллизий: квадратичное опробование.", "Условие задачи", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AskForm askForm = new AskForm();
            askForm.ShowDialog();
            if (askForm.num != "")
            {
                Tabel t = table.Search(askForm.num);
                if (t == null)
                {
                    MessageBox.Show("Табельный номер не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    TabelForm tabelForm = new TabelForm(t);
                    tabelForm.ShowDialog();
                    if (tabelForm.isRightClosed)
                    {

                        if (tabelForm.t.tabNumber == "" || tabelForm.t.FIO == "")
                        {
                            MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            table.Delete(t);
                            table.Add(tabelForm.t);
                            TableMake();
                        }
                    }
                }
            }
        }
    }
}
