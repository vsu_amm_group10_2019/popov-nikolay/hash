﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hash
{
    public partial class TabelForm : Form
    {
        public TabelForm()
        {
            InitializeComponent();
            tabNumberTextBox.Text = "";
            FIOTextBox.Text = "";
            salaryTextBox.Text = "0";
        }

        public TabelForm(Tabel tt)
        {
            InitializeComponent();
            tabNumberTextBox.Text = tt.tabNumber;
            tabNumberTextBox.ReadOnly = true;
            FIOTextBox.Text = tt.FIO;
            salaryTextBox.Text = tt.salary.ToString();
        }
        public Tabel t;
        public bool isRightClosed = false;

        private void button1_Click(object sender, EventArgs e)
        {
            isRightClosed = true;
            t = new Tabel();
            t.tabNumber = tabNumberTextBox.Text;
            t.FIO = FIOTextBox.Text;
            if (String.IsNullOrWhiteSpace(salaryTextBox.Text))
            {
                t.salary = 0;
            }
            else
            {
                try
                {
                    t.salary = Convert.ToDouble(salaryTextBox.Text);
                }
                catch
                {
                    MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    isRightClosed = false;
                }
            }
            Close();
        }
    }
}
