﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hash
{
    class HashTable
    {
        struct Item
        {
            public Tabel value;
            public bool wasDeleted;
        }
        Item[] items;
        const int c = 5, d = 7;

        public HashTable(int size)
        {
            items = new Item[size];
            Clear();
        }

        public int GetSize()
        {
            return items.Length;
        }

        public void Clear()
        {
            for (int i = 0; i < items.Length; i++)
            {
                items[i].value = null;
                items[i].wasDeleted = false;
            }
        }

        public void Add (Tabel item)
        {
            int hash = GetHash(item.tabNumber), i = 0,
                 key = hash % items.Length;
            while (i < items.Length && items[key].value != null)
            {
                i++;
                key = (hash + c * i + d * i * i) % items.Length;
            }
            if (items[key].value == null)
            {
                items[key].value = item;
                items[key].wasDeleted = false;
            }
        }

        public void Delete (Tabel item)
        {
            int hash = GetHash(item.tabNumber), i = 0,
                key = hash % items.Length;
            while (items[key].value != null || items[key].wasDeleted)
            {
                if (items[key].value != null && items[key].value == item)
                {
                    items[key].value = null;
                    items[key].wasDeleted = true;
                    return;
                }
                i++;
                key = (hash + c * i + d * i * i) % items.Length;
            };
        }

        public Tabel Search (string tabNum)
        {
            int hash = GetHash(tabNum), i = 0, 
                key = hash % items.Length;
            while (items[key].value != null || items[key].wasDeleted)
            {
                if (items[key].value != null && items[key].value.tabNumber == tabNum)
                {
                    return items[key].value;
                }
                else i++;
                key = (hash + c * i + d * i * i) % items.Length;
            }
            return null;
        }

        public List<Tabel> GetAll()
        {
            List<Tabel> list = new List<Tabel>();
            foreach (Item i in items)
            {
                if (i.value != null)
                {
                    list.Add(i.value);
                }
            }
            return list;
        }

        public int GetHash(string tabNum)
        {
            int result = 0;
            foreach (char c in tabNum)
            {
                result += c;
            }
            return result;
        }
    }
}
